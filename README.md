# Prova VIVO Linux

This project is aimed to show a model of server/client connection, where this client will request hardware usage information to the server.
Such information available in the server are Total RAM memory, RAM used and a list of 10 process that are consuming more CPU in real time.

Some dependencies are demanded to run this program in your Linux machine:

- G++ standard compiler and libraries:
To install the libraries just in case your system does not have them embedded:
      $ sudo apt install build-essential

- Visual Studio Code was used with a compiler and programming interface, https://code.visualstudio.com/.
You can opt for loading the folders SERVER and CLIENT into Visual Studio Code. Compile first SERVER then CLIENT.
Once the binaries are generated, run them via terminal (shell) using ./SERVER and check if there is no error message.
After that, run ./CLIENT and the socket connection will be established automatically, then you can select the information to be collected via interface.
