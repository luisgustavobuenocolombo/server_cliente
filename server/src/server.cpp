#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <iostream>
#include <vector>
#include <dirent.h>
#include <ctype.h>
#include <stdio.h>
#include "stdlib.h"
#include <chrono>
#include <unistd.h>
#include <algorithm>

#define PORT 8000
#define BUFFER_LENGTH 4096

using namespace std;
std::string getPidCPUusage ();
void init_CPUusage();
double getCurrentValue();

typedef struct MEMPACKED         
{  
char name1[20];       
unsigned long MemTotal;  
char name2[20];  
unsigned long MemFree;  
char name3[20];  
unsigned long Buffers;  
char name4[20];  
unsigned long Cached;  

}MEM_OCCUPY;  

int get_meminfo(MEM_OCCUPY * lpMemory);

struct pidInfo
{
    unsigned long int uTime;
    unsigned long int sTime;
    unsigned long int cuTime;
    unsigned long int csTime;
    unsigned long int startTime;
    float cpuUsage;
    std::string pid;
};

static unsigned long long lastTotalUser, lastTotalUserLow, lastTotalSys, lastTotalIdle;
int main(void) 
{
    struct sockaddr_in client, server;
    int serverfd, clientfd;
    char buffer[BUFFER_LENGTH];
    MEM_OCCUPY ramInfo;

    fprintf(stdout, "Starting server\n");

    serverfd = socket(AF_INET, SOCK_STREAM, 0);
    if(serverfd == -1) {
        perror("Can't create the server socket:");
        return EXIT_FAILURE;
    }
    fprintf(stdout, "Server socket created with fd: %d\n", serverfd);

    server.sin_family = AF_INET;
    server.sin_port = htons(PORT);
    memset(server.sin_zero, 0x0, 8);

    int yes = 1;
    if(setsockopt(serverfd, SOL_SOCKET, SO_REUSEADDR,&yes, sizeof(int)) == -1) 
    {
        perror("Socket options error:");
        return EXIT_FAILURE;
    }

    if(bind(serverfd, (struct sockaddr*)&server, sizeof(server)) == -1 )
    {
        perror("Socket bind error:");
        return EXIT_FAILURE;
    }

    if(listen(serverfd, 1) == -1) 
    {
        perror("Listen error:");
        return EXIT_FAILURE;
    }
    fprintf(stdout, "Listening on port %d\n", PORT);

    socklen_t client_len = sizeof(client);
    if ((clientfd=accept(serverfd,(struct sockaddr *) &client, &client_len )) == -1) 
    {
        perror("Accept error:");
        return EXIT_FAILURE;
    }

    strcpy(buffer, "Hello, client!\n\0");

    if (send(clientfd, buffer, strlen(buffer), 0)) 
    {
        fprintf(stdout, "Client connected.\nWaiting for client message ...\n");
        do {
            memset(buffer, 0x0, BUFFER_LENGTH);

            int message_len;
            if((message_len = recv(clientfd, buffer, BUFFER_LENGTH, 0)) > 0)
            {
                buffer[message_len - 1] = '\0';
                printf("Client request %s\n", buffer);

                if (buffer[0] == '1')
                {
                    get_meminfo(&ramInfo);
                    string ramusage = to_string(ramInfo.MemTotal);
                    send(clientfd, ramusage.c_str(), 25, 0);
                }
                if (buffer[0] == '2')
                {
                    get_meminfo(&ramInfo);
                    string ramusage = to_string(ramInfo.MemTotal - ramInfo.MemFree);
                    send(clientfd, ramusage.c_str(), 25, 0);
                }
                if (buffer[0] == '3')
                {                   
                    string cpuusage = to_string(getCurrentValue());
                    init_CPUusage();
                    getCurrentValue();
                    send(clientfd, cpuusage.c_str(), 15, 0);
                }
                if (buffer[0] == '4')
                {   
                    string output = getPidCPUusage();
                    cout << endl; 
                    cout << output.c_str();
                    usleep(10000);
                    send(clientfd, output.c_str(), output.size(), 0);
                }
            }
        } while(strcmp(buffer, "exit"));
    }

    close(clientfd);
    close(serverfd);
    printf("Connection closed\n\n");
    return EXIT_SUCCESS;
}

void init_CPUusage()
{
    FILE* file = fopen("/proc/stat", "r");
    fscanf(file, "cpu %llu %llu %llu %llu", &lastTotalUser, &lastTotalUserLow,
        &lastTotalSys, &lastTotalIdle);
    fclose(file);
}

double getCurrentValue(){
    double percent;
    FILE* file;
    unsigned long long totalUser, totalUserLow, totalSys, totalIdle, total;

    file = fopen("/proc/stat", "r");
    fscanf(file, "cpu %llu %llu %llu %llu", &totalUser, &totalUserLow,
        &totalSys, &totalIdle);
    fclose(file);

    if (totalUser < lastTotalUser || totalUserLow < lastTotalUserLow ||
        totalSys < lastTotalSys || totalIdle < lastTotalIdle){
        //Overflow detection. Just skip this value.
        percent = -1.0;
    }
    else{
        total = (totalUser - lastTotalUser) + (totalUserLow - lastTotalUserLow) +
            (totalSys - lastTotalSys);
        percent = total;
        total += (totalIdle - lastTotalIdle);
        percent /= total;
        percent *= 100;
    }

    lastTotalUser = totalUser;
    lastTotalUserLow = totalUserLow;
    lastTotalSys = totalSys;
    lastTotalIdle = totalIdle;

    return percent;
}

using std::cout; using std::cin;
using std::endl; using std::vector;

float calculateCPUUsage(pidInfo &pid, long int clock, unsigned long long int uptime)
{
    unsigned long long total_time = pid.uTime + pid.sTime;
    float seconds = uptime - float(pid.startTime/clock);
    float cpuUsage = 100 * float(float(total_time / clock) / seconds);
    return cpuUsage;
}

bool is_digits(const std::string &str)
{
    for(int i = 0; i < str.size(); i++)
    {
        char c = str.at(i);
        if(isdigit(c) == 0)
            return false;
    }
    return true;
}

bool getTimesForPID(pidInfo &info)
{
    std::string path = "/proc/";
    path.append(info.pid);
    path.append("/stat");

    char *t; 

    FILE* file = fopen(path.c_str(), "r");

	int pid,ppid,pgrp,session,tty_nr,tpgid;
	unsigned int flags;
	unsigned long minflt,cminflt,majflt,cmajflt, foo;
	char cm[255],cpun[255],stat;

    if(file == nullptr)
    {
        cout << "Pid: " + info.pid + " not found!";
        return false;
    }
	if(fscanf(file,"%d %s %c %d %d %d %d %d %u %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu",
		&pid,cm,&stat,&ppid,&pgrp,&session,&tty_nr,&tpgid,&flags,
		&minflt,&cminflt,&majflt,&cmajflt,&(info.uTime),&(info.sTime),
        &info.cuTime, &info.csTime, &foo, &foo, &foo, &foo, &info.startTime) == EOF)
		perror("fscanf");
    fclose(file);
    return true;
}

bool getPIDInProc(std::vector<std::string> &pids)
{
    DIR *dir; 
    std::string path = "/proc/";
    struct dirent *diread;
    
    if ((dir = opendir(path.c_str())) != nullptr) 
    {
        while ((diread = readdir(dir)) != nullptr) 
        {   
            std::string pid = diread->d_name;  

            if(is_digits(pid))
                pids.push_back(pid);            
        }
        closedir (dir);
    } 
    else 
    {
        perror ("opendir");
        return false;
    }

    return true;
}

bool getPIDTimes(std::vector<pidInfo> &info, std::string pid)
{
    vector<char *> files;
    unsigned long long uptime;
    
    pidInfo current_pid;
    current_pid.pid = pid;

    //Getting uptime
    FILE* file = fopen("/proc/uptime", "r");
    fscanf(file, "%llu", &uptime);
    fclose(file);

    //Getting clock time
    long int clock = sysconf(_SC_CLK_TCK);

    if(!getTimesForPID(current_pid))
        return false;

    current_pid.cpuUsage = calculateCPUUsage(current_pid, int(clock), uptime);

    info.push_back(current_pid);

    return true;
}


std::string getPidCPUusage ()
{
    std::vector<pidInfo> pidsInfo;
    std::vector<std::string> pids;
    getPIDInProc(pids);

    for(auto pid : pids)
        getPIDTimes(pidsInfo, pid);

    std::sort(pidsInfo.begin(), pidsInfo.end(), [](const pidInfo& lhs, const pidInfo& rhs) {
    return lhs.cpuUsage > rhs.cpuUsage;});
                        
    //cout << pidsInfo.at(0).pid + "  =" << pidsInfo.at(0).cpuUsage << std::endl;
    //usleep(1000000);

    std::string CPUusagelist;
    CPUusagelist.append("\n");
    for(int i=0; i<10; i++)
    {
        CPUusagelist.append(std::to_string(i));
        CPUusagelist.append(" - PID: ");
        CPUusagelist.append(std::string (pidsInfo.at(i).pid));
        CPUusagelist.append(" - CPU usage: ");
        CPUusagelist.append(std::to_string(pidsInfo.at(i).cpuUsage));
        CPUusagelist.append("%");
        CPUusagelist.append("\n");
    }
    return CPUusagelist;
}

int  get_meminfo(MEM_OCCUPY * lpMemory)
{
  FILE *fd;
  char buff[128];
  fd = fopen("/proc/meminfo", "r"); 
  if(fd <0) return -1;
  fgets(buff, sizeof(buff), fd); 
  sscanf(buff, "%s %lu ", lpMemory->name1, &lpMemory->MemTotal); 
  fgets(buff, sizeof(buff), fd); 
  sscanf(buff, "%s %lu ", lpMemory->name2, &lpMemory->MemFree);  
  fgets(buff, sizeof(buff), fd);
  sscanf(buff, "%s %lu ", lpMemory->name3, &lpMemory->Buffers);  
  fgets(buff, sizeof(buff), fd);
  sscanf(buff, "%s %lu ", lpMemory->name4, &lpMemory->Cached); 
  
  fclose(fd);	
}

